<?php

class Ts_Promocoupon_Block_Product_View_Coupon extends Mage_Catalog_Block_Product_View_Abstract
{

    protected $model;

    public function _construct()
    {
        parent::_construct();
        $this->model = Mage::getModel('promocoupon/coupon');
    }

    /**
     * @return bool
     */
    public function isUserLogged()
    {
        return Mage::getSingleton('customer/session')->isLoggedIn();
    }

    /**
     * @return bool
     */
    public function isQtyPositive()
    {
        return $this->model->isQtyPositive($this->getProduct());
    }

    /**
     * @return string $couponCode
     */
    public function getCode()
    {
        return $this->model->generateCode($this->getProduct());
    }
    
    /**     
     * @return bool
     */
    public function isModuleEnabled(){
	  return $this->model->isModuleEnabled();
    }
}