<?php

class Ts_Promocoupon_Model_Coupon extends Mage_Core_Model_Abstract
{
    public $ruleExpireDate = '';

    protected function _construct()
    {
        parent::_construct();
        $this->_init('promocoupon/coupon');
    }

    /**
     * @param object $product
     * @return bool
     */
    public function isQtyPositive($product)
    {
        $stockItem = $product->getData('stock_item');

        if($stockItem->getData('qty') > 0){

            return true;
        }

        return false;
    }

    /**
     * TODO: handle exceptions
     * TODO: set configuration from admin panel
     * TODO: bind coupon with a specific user
     * @param object $product
     * @return string $couponCode
     */
    public function generateCode($product)
    {
        $couponGenerator = Mage::getModel('salesrule/coupon_massgenerator');

        $rule = $this->setSalesRule($product);
        $ruleId = $rule->getId();

        $couponConfig = array(
            'uses_per_customer' => $this->getCustomerId(),
            'uses_per_coupon'   => 1,
            'qty'               => 1,
            'length'            => 12,
            'to_date'           => $this->getExpireDate(),
            'prefix'            => '',
            'suffix'            => '',
            'dash'              => 5,
            'format'          => Mage_SalesRule_Helper_Coupon::COUPON_FORMAT_ALPHANUMERIC,
            'rule_id'         => $ruleId
        );
        $couponGenerator->validateData($couponConfig);
        $couponGenerator->setData($couponConfig);

        $rule->setCouponCodeGenerator($couponGenerator);
        $rule->setCouponType( Mage_SalesRule_Model_Rule::COUPON_TYPE_AUTO );

        $coupon = $rule->acquireCoupon();
        $code = $coupon->getCode();

        return $code;
    }

    /**
     * TODO: implement getCustomerGroup()
     * TODO: interrupt set rule if a product has not any related or upsell products
     * @param object $product
     * @return string $salesRuleId
     */
    public function setSalesRule($product){
        $model = Mage::getModel('salesrule/rule');

        $model->setName($this->getRuleName())
            ->setDescription($this->getRuleDescription())
            ->setFromDate('')
            ->setCouponType(2)
            ->setUsesPerCustomer(1)
            ->setCustomerGroupIds(array(1, 2, 3))
            ->setIsActive(1)
            ->setConditionsSerialized('')
            ->setActionsSerialized('')
            ->setStopRulesProcessing(0)
            ->setIsAdvanced(1)
            ->setProductIds($this->getRuleProductIds($product))
            ->setSortOrder(0)
            ->setToDate($this->getExpireDate())
            ->setSimpleAction('by_percent')
            ->setDiscountAmount($this->getDiscountRate())
            ->setDiscountQty(null)
            ->setDiscountStep(0)
            ->setSimpleFreeShipping('0')
            ->setApplyToShipping('0')
            ->setIsRss(0)
            ->setWebsiteIds(array(1));
        $model->save();

        return $model;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return Mage::getSingleton('customer/session')->getCustomer()->getId();
    }

    /**
     * @return string
     * TODO: handle discount rate is not valid (can't be converted to integer)
     */
    public function getDiscountRate()
    {
        return    Mage::getStoreConfig('promocoupon/general/coupon_discount_rate');
    }

    /**
     * TODO: implement rule name config from admin
     */
    public function getRuleName()
    {
        return 'Zero stock product discount';
    }

    /**
     * TODO: implement rule description config from admin
     */
    public function getRuleDescription()
    {
        return 'Set discount for upsale and related products if the product stock is below 1.';
    }

    /**
     * @param object $product
     * @return string $ruleProductIds
     */
    public function getRuleProductIds($product)
    {
        $ruleProductIds = [];

        if(!empty($product->getRelatedProductIds())){
            $ruleProductIds = array_merge($product->getRelatedProductIds(), $ruleProductIds);
        }

        if(!empty($product->getUpSellProductIds())){
            $ruleProductIds = array_merge($product->getUpSellProductIds(), $ruleProductIds);
        }

        return implode(',', array_unique($ruleProductIds));
    }

    /**
     * TODO: handle date is not valid
     * @return false|string
     */
    public function getExpireDate(){
        if(empty($this->ruleExpireDate)){
            $ruleLifetime = Mage::getStoreConfig('promocoupon/general/coupon_lifetime');
            $this->ruleExpireDate = date('Y-m-d H:i:s', strtotime("+$ruleLifetime hours",  time()));
        }

        return $this->ruleExpireDate;
    }
    
    public function isModuleEnabled(){
	  return Mage::getStoreConfig('promocoupon/general/enabled');
    }
}